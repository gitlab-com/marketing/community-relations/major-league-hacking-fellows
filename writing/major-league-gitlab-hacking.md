# Major league GitLab hacking

Working on [open source](https://go.gitlab.com/spHNym) projects [like GitLab](https://gitlab.com/gitlab-org) can be a powerful way to learn software development. Just ask [Mughees Pervaiz](https://gitlab.com/Mughees_), a computer science student at the University of South Asia, and [Young Jun Joo](https://gitlab.com/youngjun827), a student of mathematics and economics at the University of Waterloo. They recently contributed to GitLab as part of a fellowship with [Major League Hacking](https://mlh.io/about), a non-profit working to empowering tomorrow's technology leaders. The fellows' [12-week program just wrapped](https://fellowship.mlh.io/), but before they departed we gave them one final assignment: *Explain your favorite contribution to GitLab during your fellowship and tell readers what you learned from it.*

Here's what they had to say.

## Mughees Pervaiz

During my internship, I was a part of the GitLab Foundation team under the mentorship of our maintainer, [James Rushford](https://gitlab.com/jrushford). My primary responsibility was to improve both the developer and user experience on GitLab. 

I would say my favorite contribution would be my work helping to update expand/collapse buttons in Roadmaps [from link buttons to tertiary buttons](https://gitlab.com/gitlab-org/gitlab/-/issues/396775). Before the changes, the feature was using old components, and I updated it to new GitLab UI components. We had to [migrate the expand/collapse buttons](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117242) that appear in the roadmap tree drawer. So we were working with the tree view (or, you could say, the "child components" of the roadmap) and because of this it was very difficult for me to find the right file from GitLab's very large codebase.

Making this contribution taught me a lot about working with the GitLab codebase—specifically about making changes to the front-end user interface. I had to work with different GitLab components, such as the epic item details (vue component), epic item details (js component), and the front-end JavaScript libraries such as [jest](https://jestjs.io/), in order to  test or implement this feature.

But additionally, this contribution helped me develop my collaboration and communication skills. I had to work closely with other members of the GitLab community, including designers, product managers, and other contributors, in order to refine the feature and ensure that it aligned with GitLab's design principles and user experience goals.

I also learned not to jump into coding without understanding an issue completely. Here, James helped me quite a bit: When I asked for hsis guidance, he responded with the following questions:

- What is your understanding of the problem
- What did you try so far
- What did you find so far?
- What are your next ideas?
- Where did you look for information?

At first, I was confused. *Why is he asking me these questions instead of helping me?* But following James' technique really helped me with the issue, and I solved the problem by myself. At that moment, one of the most important lessons was clear to me: *Don't underestimate myself*. James wanted me to go beyond my limits, "get out of the box" and try to solve the issue by myself so I could have a better undersanding of what was happening in the codebase. So to anyone wishing to contribute to GitLab, I would say: Believe in yourself and give your best, and make sure you read the issue closely to develop a good understanding of the problem you're going to solve.

## Young Jun Joo

During my MLH fellowship with GitLab, I worked on [improving the GitLab search function](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111741). Some GitLab users were experiencing a slow search experience—and as a user myself, I understood the importance of having a quick and efficient search function. I wanted to help make that a reality for other users.

So I [implemented a memoization algorithm](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112606) that caches search results. Now, if a user searches for the same word multiple times, the search results will be retrieved from the cache instead of being recalculated each time. This results in a faster and more efficient search experience for GitLab users.

My contribution to GitLab was significant because it improved the search function's performance and efficiency, making it more reliable and user-friendly. Working on this project taught me a lot about optimization and efficiency in software development. I also gained a deeper understanding of how memoization algorithms work and how they can be used to improve performance.

My work on this project not only was extremely rewarding but also had a positive impact on GitLab and its users, who can now quickly and easily search for the information they need without experiencing lag or delays. I'm proud to have made a meaningful contribution to such a valuable tool for software development, and grateful for the opportunity to have worked with such a talented team of developers.
